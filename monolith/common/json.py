# Encoders belong here
from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


# resolves date related items
class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


# allows us to see list of conferences
class QuerySetEncoder(JSONEncoder):
    def default(self, o): # o is an instance of a QuerySet
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)






""" 
Pseudocode for the encoder ModelEncoder

* if the object to decode is the same class as what's in the
  model property, then
    * create an empty dictionary that will hold the property names
      as keys and the property values as values
    * for each name in the properties list
        * get the value of that property from the model instance
          given just the property name
        * put it into the dictionary with that property name as
          the key
    * return the dictionary
"""

class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model): # if object instance to decode is same class as what is in model property
            d = {} # holds property names as keyes, and property values as values
            if hasattr(o, "get_api_url"): # if there is the attribute get_api_url
                d["href"] = o.get_api_url() # add that value to the dictionary
            for property in self.properties: # for each name in property list
                value = getattr(o, property) # get value of property from the instance, given the property name
                if property in self.encoders: # look at ConferenceDetailEncoder -> encoders variable with location:LocationListEncoder()
                    encoder = self.encoders[property] # property would be 'location'
                    value = encoder.default(value) # creates dictionary with the {href: 'api..', 'name': '...'}
                d[property] = value # put this into dictionary with that property name as the key
            d.update(self.get_extra_data(o)) # update the dictionary with another dictionary
            return d # return dictionary
        else:
            return super().default(o) # boilerplate from documentation

    def get_extra_data(self, o):
        return {} # why is this just an empty dictionary?


# Model Encoder built during class 8/16/2022
# class ModelEncoder(JSONEncoder):
#     encoders = {}

#     def default(self, o):  # self and object
#         if isinstance(o, self.model):
#             d = {}
#             if hasattr(o, "get_api_url"):
#                 d["href"] = o.get_api_url()
#             for property in self.properties:  # iterate thru each one of these properties
#                 value = getattr(o, property)  # built in python fxn
#                 if property in self.encoders:
#                     encoder = self.encoders[property]
#                     value = encoder.default(value)
#                 d[property] = value
#             d.update(self.get_extra_data(o))
#             return d
#         else:
#             return super().default(o)

#     def get_extra_data(self, o):
#         return {}
