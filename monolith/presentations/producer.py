import pika
import json


name = presentation.presenter_name
email = presentation.presenter_email
title = presentation.title

parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue="presentation_approvals")
channel.basic_publish(
    exchange="",
    routing_key="presentation_approvals",
    body=json.dumps(
        {
            "presenter_name": name,
            "presenter_email": email,
            "title": title
        }
    )
)

channel.queue_declare(queue="presentation_rejections")

channel.basic_publish(
    exchange="",
    routing_key="presentation_rejections",
    body=json.dumps(
        {
            "presenter_name": name,
            "presenter_email": email,
            "title": title
        }
    )
)
connection.close()
