* [x] install requests
* [x] pip freeze requirements.txt
* [] Update for when create Location to get url to a picture of that city
* [x] add acls.py file in events app
  * add code that will make the http requests to pexel and weather
  * at top of acls.py import to use keys:
    [x]from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
    use pexels using api key
  * [] will need header for request
      * [] header name = “Authorization”
      * [] header value = “{API key}”
      * https://learn-2.galvanize.com/cohorts/3352/blocks/1873/content_files/build/04-hello-acls/66-integrating-third-party-data.md#:~:text=request.%20Here%27s%20a-,link%20to%20the%20example,-that%20shows%20how
        * instead of “user-agent” will be “Authorization”
  * To use API key:
  * [x] in .gitignore file add events/keys.py near top
  * [x] create events/keys.py file and put pexel and weather api keys in
  * [] Update Location model(?) to include “picture_url”
    * [] None/null for existing
    * [] If no url can be found - null
* [] Update Conference details to get weather data for city
    use Geocoding API to convert city to Latitude and Longitude
    Find current weather data for latitude longitude using above
    * [] update model(?) if weather data available to include “temp” and “description” -
    * [] if no data “weather”: null


# setup for the Conference_weather functionality
*. update model
1. create keys.py
2. add keys to gitignore.com
3. create [acls.py](http://acls.py) (anti-corruption layer)
    1. _sepeartes file to process any external calls we make to an external api_
    2. setup headers, parameters, url
       1. headers: based on documentation
       2. parameters -> look at documentation for input parameters
       3. url -> look at docs. the get url. do not include the '?'
    3. response = requests.get(url, params=params, headers = header)
    4. turn response into python using json.loads
       1. content = json.loads(response.content)
    5. try / except : to return either the apporpriate photo, or a None response
4. update encoder to take that new parameters ( if needed)
5. update the view function, to include this new information in content
   1. we are just getting the information from the weather API, but not saving it in the database
      1. therefore no model updates are required

