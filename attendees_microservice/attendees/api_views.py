from xml.sax.xmlreader import AttributesNSImpl
from xmlrpc.client import Boolean
from django.http import JsonResponse
from common.json import ModelEncoder
# from events.api_views import ConferenceVODetailEncoder
from django.views.decorators.http import require_http_methods 
# from events.models import Conference
import json
from django.db import IntegrityError

from .models import Attendee, ConferenceVO, AccountVO

class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeesDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        # "conference",  # conference is a dictionary
    ]
    encoders = {"conference": ConferenceVODetailEncoder()} # used to encode conference 

    def get_extra_data(self, o):
        count = AccountVO.objects.filter(email=o.email).count()
        if count > 0:
            return {"has_account": True}
        else:
            return {"has_account": False}

class AttendeesListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]



@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
# without encoder
    # response = []
    # attendees = Attendee.objects.filter(conference=conference_id)
    # for attendee in attendees:
    #     response.append(
    #         {"name": attendee.name, "href": attendee.get_api_url()}
    #     )

    # return JsonResponse({"attendees": response})

# with encoder
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse({"attendees": attendees}, encoder=AttendeesListEncoder)
    else: # handles POST request
        content = json.loads(request.body)

        # get conference object, and put it in content dictionary
        try:
            conference_href = f'/api/conferences/{conference_vo_id}/'
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "invalid conference id"}, status=400)
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeesDetailEncoder,
            safe=False,)

@require_http_methods(["DELETE","GET", "PUT"])
def api_show_attendee(request, pk):
    """
    Returns the details for the Attendee model specified
    by the pk parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
# without encoder
    # attendee = Attendee.objects.get(id=pk)
    # return JsonResponse(
    #     {
    #         "email": attendee.email,
    #         "name": attendee.name,
    #         "company_name": attendee.company_name,
    #         "created": attendee.created,
    #         "conference": {
    #             "name": attendee.conference.name,
    #             "href": attendee.conference.get_api_url(),
    #         },
    #     }
    # )

# with encoder 
    if request.method == "GET":
        attendee = Attendee.objects.get(id=pk)
        return JsonResponse(attendee, encoder=AttendeesDetailEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=pk).delete()
        return JsonResponse({"deleted":count > 0})
    else: # handles PUT method
        content = json.loads(request.body)
        try: # ensures that conferences you may update, are in the list of existing conferences
            if "conference" in content: # if conference is something they are trying to update
                conference = Conference.objects.get(id=content["conference"])  #get the conference information based on the id that is PUT
                content["conference"] = conference # update content, with that new conference value
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "invalid conference information"}, status=400)
        Attendee.objects.filter(id=pk).update(**content) # updates the object
        attendee = Attendee.objects.get(id=pk) # get that object and assign it to attendee
        return JsonResponse(
            attendee,
            encoder=AttendeesDetailEncoder,
            safe=False,)
