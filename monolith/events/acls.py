import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY # from current directly, keys file -> import the variables


def get_photo(city, state):

    # start by defining your header!
    headers = {"Authorization": PEXELS_API_KEY} # auth is for the api key
    # establish parameters -> based on the docs for the API
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    # get the urls here
    url = "https://api.pexels.com/v1/search"

    # now make that request
    response = requests.get(url, params=params, headers=headers)
    # turn our response into a python value
    content = json.loads(response.content)

    # create an object that represents the data we are getting back

    # use try/except -> to see if we get information back! if we don't get soemthing back, throw error
    # this is where we actually return the response
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}

    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather(city, state):
    
    params = {
        "q": city + "," + state + "," + "US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1
    }

    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    parameters = {
        "lat": content[0]['lat'],
        "lon": content[0]['lon'],
        "appid": OPEN_WEATHER_API_KEY,
        "units": 'imperial'
    }

    wurl = "https://api.openweathermap.org/data/2.5/weather"
    wresponse = requests.get(url=wurl, params=parameters)
    weather = json.loads(wresponse.content)
    temp = weather['main']['temp']
    desc = weather['weather'][0]['description']
    try:
        return {"temp": temp, "description": desc}

    except (KeyError, IndexError):
        return {"weather": None}